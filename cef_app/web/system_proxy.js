// SPDX-License-Identifier: GPL-2.0+

// Copyright © 2022 Collabora Ltd
// Copyright © 2022 Valve Corporation

async function trigger(url) {
    let err_msg = ""
    let result_json = ""
    try {
        let response = await fetch(url, {
            mode: "no-cors"
        })
        console.log("trigger: " + url + " : " + JSON.stringify(response));
        if (response.ok) {
            let result_json = await response.json();
            return Promise.resolve(result_json);
        } else {
            err_msg = response.statusText;
            console.log("Response error : " + response.statusText);
        }
    } catch (err) {
        err_msg = err.message;
    }
    return Promise.reject(err_msg);
}

class SystemProxy {
    constructor(server) {
        this.server = server;
    }

    triggerScan() {
        let url = this.server + '/scan';
        let status = {
            state: 'NULL', //"NULL", "PENDING", "FINISHED", "FAILED"
            result: '',
            message: ''
        };
        return trigger(url)
            .then(
                (result) => {
                    if (result.status == 200) {
                        status.state = 'FINISHED'
                    } else if (result.status == 102) {
                        status.state = 'PENDING'
                    } else {
                        status.state = 'FAILED'
                    }
                    status.result = result;
                    status.message = result.message;
                    return status;
                },
                (err) => {
                    status.state = 'FAILED'
                    status.message = err;
                    return status;
                });
    }

    triggerList(id) {
        let url = this.server + '/list' +
            '?uuid=' + id;
        let status = {
            'state': 'NULL', //"NULL", "PENDING", "FINISHED", "FAILED"
            'result': '',
            'message': ''
        };
        return trigger(url)
            .then(
                function(result) {
                    if (result.status == 200) {
                        status.state = 'FINISHED'
                    } else if (result.status == 102) {
                        status.state = 'PENDING'
                    } else {
                        status.state = 'FAILED'
                    }
                    status.result = result;
                    status.message = result.message;
                    return status;
                },
                function(err) {
                    status.state = 'FAILED'
                    status.message = err;
                    return status;
                });
    }

    triggerRestore(discName, imageName, scanuuid) {
        let url = this.server + "/restore" + "?disc=" + discName +
            "&image=" + imageName + "&scanuuid=" + scanuuid;
        let status = {
            "state": "NULL", //"NULL", "PENDING", "FINISHED", "FAILED"
            "result": "",
            "message": ""
        };
        return trigger(url).then(function(result) {
                if (result.status == 200) {
                    status.state = "FINISHED"
                } else if (result.status == 102) {
                    status.state = "PENDING"
                } else {
                    status.state = "FAILED"
                }
                status.result = result;
                status.message = result.message;
                return status;
            },
            function(err) {
                status.state = "FAILED"
                status.message = err;
                return status;
            });
    }

    triggerRepair(discName, imageName, scanuuid) {
        let url = this.server + "/prepare-chroot" + "?disc=" + discName +
            "&image=" + imageName + "&scanuuid=" + scanuuid;
        let status = {
            "state": "NULL", //"NULL", "PENDING", "FINISHED", "FAILED"
            "result": "",
            "message": ""
        };
        return trigger(url).then(function(result) {
                if (result.status == 200) {
                    status.state = "FINISHED"
                } else if (result.status == 102) {
                    status.state = "PENDING"
                } else {
                    status.state = "FAILED"
                }
                status.result = result;
                status.message = result.message;
                return status;
            },
            function(err) {
                status.state = "FAILED"
                status.message = err;
                return status;
            });
    }

    triggerFactoryReset(discName, imageName, scanuuid) {
        let url = this.server + "/factory-reset" + "?disc=" + discName +
            "&image=" + imageName + "&scanuuid=" + scanuuid

        let status = {
            "state": "NULL", //"NULL", "PENDING", "FINISHED", "FAILED"
            "result": "",
            "message": ""
        };
        return trigger(url).then(function(result) {
                if (result.status == 200) {
                    status.state = "FINISHED"
                } else if (result.status == 102) {
                    status.state = "PENDING"
                } else {
                    status.state = "FAILED"
                }
                status.result = result;
                status.message = result.message;
                return status;
            },
            function(err) {
                status.state = "FAILED"
                status.message = err;
                return status;
            });
    }

    triggerReboot(discName, imageName, scanuuid, delay) {
        let url = this.server + "/reboot" + "?disc=" + discName +
            "&image=" + imageName + "&scanuuid=" +
            scanuuid + "&delay=" + delay;

        let status = {
            "state": "NULL", //"NULL", "PENDING", "FINISHED", "FAILED"
            "result": "",
            "message": ""
        };
        return trigger(url).then(function(result) {
                if (result.status == 200) {
                    status.state = "FINISHED"
                } else if (result.status == 102) {
                    status.state = "PENDING"
                } else {
                    status.state = "FAILED"
                }
                status.result = result;
                status.message = result.message;
                return status;
            },
            function(err) {
                status.state = "FAILED"
                status.message = err;
                return status;
            });
    }


    getStatusAll() {
        let status = {
            "state": "NULL", //"NULL", "PENDING", "FINISHED", "FAILED"
            "result": "",
            "message": ""
        };

        let url = this.server + "/reboot" + "?image=" + imageName;

        return trigger(url).then(function(result) {
                if (result.status == 200) {
                    status.state = "FINISHED"
                } else if (result.status == 102) {
                    status.state = "PENDING"
                } else {
                    status.state = "FAILED"
                }
                status.result = result;
                status.message = result.message;
                return status;
            },
            function(err) {
                status.state = "FAILED"
                status.message = err;
                return status;
            });
    }

    getStatusById(id, start, max) {
        let url = this.server + "/status" +
            "?uuid=" + id + "&start=" + start + "&max=" + max;
        let status = {
            "state": "NULL", //"NULL", "PENDING", "FINISHED", "FAILED"
            "result": "",
            "message": ""
        };

        return trigger(url).then(function(result) {
                if (result.status == 200) {
                    status.state = "FINISHED"
                } else if (result.status == 102) {
                    status.state = "PENDING"
                } else {
                    status.state = "FAILED"
                }
                status.result = result;
                status.message = result.message;
                return status;
            },
            function(err) {
                status.state = "FAILED"
                status.message = err;
                return status;
            });
    }

    triggerClearLog(uuid) {
        let url = this.server + "/clear" +
            "?uuid=" + uuid;
        let status = {
            "state": "NULL", //"NULL", "PENDING", "FINISHED", "FAILED"
            "result": "",
            "message": ""
        };

        return trigger(url).then(function(result) {
                if (result.status == 200) {
                    status.state = "FINISHED"
                } else if (result.status == 102) {
                    status.state = "PENDING"
                } else {
                    status.state = "FAILED"
                }
                status.result = result;
                status.message = result.message;
                return status;
            },
            function(err) {
                status.state = "FAILED"
                status.message = err;
                return status;
            });
    }
}

export {
    SystemProxy
};
