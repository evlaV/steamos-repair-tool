// SPDX-License-Identifier: GPL-2.0+

// Copyright © 2022 Collabora Ltd
// Copyright © 2022 Valve Corporation

import {
    SystemProxy
} from './system_proxy.js';
import {
    exConfirmPromise as dialog
} from './exConfirm.js'

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function sendMessage(msgName) {
    //Results in a call to the OnQuery method in client_impl.cc.
    window.cefQuery({
        request: msgName,
        onSuccess: function(response) {
          console.log("santo" +  response);
        },
        onFailure: function(error_code, error_message) {
          console.log("santo" +  "Failed");
        }
    });
}

const MAX_TRY_SCAN = 1000;

class System {
    constructor() {
        this.systemProxy = new SystemProxy("http://127.0.0.1:8080/steamos-repair");
        this.scanData = {};
        this.listData = {};
        this.currentActionReq = {};
    }

    async Scan() {
        let response = await this.systemProxy.triggerScan();
        this.scanData = response;
        console.log(JSON.stringify(response));

        let failed = false;
        if (response.state == "FAILED") {
            console.log("Error: " + "Scanning failed " + JSON.stringify(response.message));
            failed = true;
        } else if (response.state == "PENDING") {
            let scan_uuid = this.scanData.result.uuid;
            let count = 0;
            while (count < MAX_TRY_SCAN) {
                await sleep(1000);
                let response = await this.systemProxy.getStatusById(scan_uuid, count, 1);
                console.log(JSON.stringify(response))
                if (response.state == "FINISHED") {
                    let status_list = response.result["status_list"];
                    let status_message = status_list[scan_uuid];
                    let status_code = status_message[0];
                    let status_type = status_message[0];
                    let status_log = status_message[2];
                    updateScanStatus(status_log);
                    if (status_code == 200) {
                        updateScanStatus("Scan: Scan finished");
                        this.List(scan_uuid);
                        return;
                    } else if (status_code >= 400) {
                        failed = true;
                        break;
                    } else {
                        console.log("Scanning log: " + status_type + ":" + status_log);
                    }
                }
                count++;
            }
            failed = true;
        } else if (response.state == "FINISHED") {
            updateScanStatus("Scan: Scan finished");
            this.List(this.scanData.result.uuid);
        }

        if (failed) {
            updateScanStatus("<h1>Failed: </h1> <p>Restart application</p>");
            hideLoadingIcon();
        }

    }

    async List(id) {
        let response = await this.systemProxy.triggerList(id);
        this.listData = response;

      console.log("List: " + JSON.stringify(response));
        if (response.state == "FAILED") {
            updateScanStatus("<h1>Failed: </h1> <p>Restart application</p>");
        } else if (response.state == "PENDING") {
            // FIXME(Santo): not sure whether it can be pending
        } else if (response.state == "FINISHED") {
            let os_list = response.result["os_list"];
            if (Object.keys(os_list).length == 0) {
              updateScanStatus("<h1>Failed: </h1> <p><b>No image found<b></p>");
              return;
            }
            updateOptionMenu(os_list);
        }
    }

    async checkStatusForAction(uuid) {
        let count = 0;
        let log_count = 0;
        while (count < 5000) { // change this to very large number i.e timeout
            await sleep(2000);
            let response = await this.systemProxy.getStatusById(uuid, log_count, 100);
            console.log("status call" + JSON.stringify(response))
            if (response.state == "FINISHED") {
                let log_messages = response.result["log_messages"];
                log_count += log_messages.length;
                let status_list = response.result["status_list"];
                let status_message = status_list[uuid];
                let status_code = status_message[0];
                let status_type = status_message[1];
                let status_log = status_message[2];
                updateActionMessage(status_log);
                updateLogOutput(log_messages);
                if (status_code == 200) {
                    return [status_code, status_log];
                } else if (status_code >= 400) {
                    return [status_code, status_log];
                } else {
                    console.log("Action: running: log: " + status_type + ":" + status_log);
                }
            }
            count++;
        }
        return [408, "Timeout"];
    }

    async Restore(disc, img) {
        let current_scan_uuid = this.scanData.result.uuid;
        let response = await this.systemProxy.triggerRestore(disc, img, current_scan_uuid);
        this.restoreData = response;
        this.currentActionReq = this.restoreData;

        updateLogOutput([response.message]);
        if (response.state == "FAILED") {
            console.log("Error: " + "Restore failed " + JSON.stringify(response.message));
            RestoreFailed(response.message);
        } else if (response.state == "PENDING") {
            let restore_uuid = this.restoreData.result.uuid;
            let status = await this.checkStatusForAction(restore_uuid);
            console.log(status);
            let code = status[0]
            let msg = status[1];
            if (code == 200) {
                RestoreFinished(msg);
            } else {
                RestoreFailed(msg);
            }
        } else if (response.state == "FINISHED") {
            RestoreFinished(response.message);
        }
    }

    async FactoryReset(disc, img) {
        let current_scan_uuid = this.scanData.result.uuid;
        let response = await this.systemProxy.triggerFactoryReset(disc, img, current_scan_uuid);
        this.resetData = response;
        this.currentActionReq = this.resetData;

        updateLogOutput([response.message]);
        if (response.state == "FAILED") {
            console.log("Error: " + "Reset failed " + JSON.stringify(response.message));
            ResetFailed(response.message);
        } else if (response.state == "PENDING") {
            let reset_uuid = this.resetData.result.uuid;
            let status = await this.checkStatusForAction(reset_uuid);
            console.log(status);
            let code = status[0]
            let msg = status[1];
            if (code == 200) {
                ResetFinished(msg);
            } else {
                ResetFailed(msg);
            }
        } else if (response.state == "FINISHED") {
            ResetFinished(response.message);
        }
    }

    async Repair(disc, img) {
        let current_scan_uuid = this.scanData.result.uuid;
        let response = await this.systemProxy.triggerRepair(disc, img, current_scan_uuid);
        this.repairData = response;
        this.currentActionReq = this.repairData;

        updateLogOutput([response.message]);
        if (response.state == "FAILED") {
            console.log("Error: " + "Repair failed " + JSON.stringify(response.message))
            RepairFailed(response.message);
        } else if (response.state == "PENDING") {
            let repair_uuid = this.repairData.result.uuid;
            let status = await this.checkStatusForAction(repair_uuid);
            console.log(status)
            let code = status[0];
            let msg = status[1];
            if (code == 200) {
                msg = "Minimize this window  and repair manually with caution";
                RepairFinished(msg);
            } else {
                RepairFailed(msg);
            }
        } else if (response.state == "FINISHED") {
            let msg = "Minimize this and repair Manually with caution";
            RepairFinished(msg);
        }
    }

    async Clear() {
        let current_req_uuid = this.currentActionReq.result.uuid;
        let response = await this.systemProxy.triggerClearLog(current_req_uuid);
        if (response.state == "FAILED") {
            console.log("Error: " + "Clear logging failed " + JSON.stringify(response.message));
            updateActionMessage("Clear logs failed");
        } else if (response.state == "FINISHED") {
            updateActionMessage("Logs cleaned");
        }
    }

    async Reboot(disc, img, delay) {
        let current_scan_uuid = this.scanData.result.uuid;
        let response = await this.systemProxy.triggerReboot(disc, img, current_scan_uuid, delay);
        this.rebootData = response;
        console.log(JSON.stringify(response))

        updateLogOutput([response.message]);
        if (response.state == "FAILED") {
            console.log("Error: " + "Restore failed " + JSON.stringify(response.message));
            RebootFailed(response.message);
        } else if (response.state == "FINISHED") {
            updateActionMessage("Rebooting..");
        } else if (response.state == "PENDING") {
            let reboot_uuid = this.rebootData.result.uuid;
            let status = await this.checkStatusForAction(reboot_uuid);
            console.log(status);
            let code = status[0]
            let msg = status[1];
            if (code == 200) {
                ResetFinished(msg);
            } else {
                ResetFailed(msg);
            }
        }
    }
} //end class system

function updateLogOutput(logList) {
    let currentLog = document.getElementById("action_log").textContent;
    logList.forEach(function(item, index) {
        if (!currentLog.includes(item))
            document.getElementById("action_log").textContent += item + "\n";
    });
}

function updatePage(status) {
    document.getElementById("action_next").style.display = "block";
    hideActionIcon();
    if (status == "PASS") {
        document.getElementById("action_img").src = "pass.png";
    }
    if (status == "FAIL") {
        document.getElementById("action_img").src = "fail.png";
    }
}

function updateActionMessage(status_log) {
    document.getElementById("action_msg").textContent = status_log.replaceAll("\'", "");
}

function OnSuccess(status) {
    updateActionMessage("SUCCESS  " + status);
    updatePage("PASS");
}

function OnFail(status) {
    updateActionMessage("FAIL:  " + status);
    updatePage("FAIL");
}

// Use only one function no seperate implementation are there.
function RebootFinished(status) {
    OnSuccess(status);
}

function RebootFailed(status) {
    OnFail(status);
}

function RestoreFinished(status) {
    OnSuccess(status);
}

function RestoreFailed(status) {
    OnFail(status);
}

function ResetFinished(status) {
    OnSuccess(status);
}

function ResetFailed(status) {
    OnFail(status);
}

function RepairFinished(status) {
    OnSuccess(status);
}

function RepairFailed(status) {
    OnFail(status);
}

function hideActionIcon() {
    document.getElementById("action_img");
}

function hideLoadingIcon() {
    document.getElementById("loading_img").style.display = "none";
}

function closeLoadingIconWithDelay() {
    setTimeout(() => {
        document.getElementById("loading_id").remove();
    }, 1000);
}

function updateScanStatus(status_log) {
    document.getElementById("scan_msg").innerHTML = "<h5>" + status_log + "</h5>";
}

function updateOptionMenu(list) {
    let selectmenu = document.getElementById("images");
    while (selectmenu.firstChild)
        selectmenu.removeChild(selectmenu.lastChild);

    let opt = document.createElement('option');
    opt.value = "";
    opt.innerHTML = "Select.."
    selectmenu.appendChild(opt);

    Object.keys(list).forEach(i => {
        console.log(i, list[i]);
        let option_title = "" + i + ":";
        option_title += list[i].disc;
        Object.keys(list[i].images).forEach(k => {
            let opt = document.createElement('option');
            opt.value = JSON.stringify({
                "disc": i,
                "image": k
            });
            opt.innerHTML = option_title + " : " + k + "  ";
            selectmenu.appendChild(opt);
        });
    });
    closeLoadingIconWithDelay();
}

function show_status(status) {
    document.getElementById("status").innerHTML = status;
}

let recovery_options_ids = ["restore", "factory-reset", "repair", "reboot"];

function clear_options() {
    // clear other selected option
    for (let opt in recovery_options_ids) {
        let option = recovery_options_ids[opt];
        if (option != selected_option.id) {
            document.getElementById(option).checked = false;
        }
    }
}

function showActionPage(status) {
    //document.getElementById("main_menu").style.display= "none";
    document.getElementById("action_ui").style.display = "block";
    document.getElementById("action_label").innerHTML = status;
}

function hideOperationStatus() {
    document.getElementById("main_menu").style.display = "block";
    document.getElementById("operation_status").style.display = "none";
    document.getElementById("action_type").innerHTML = "";
}

let selectedImage = ""
export function ImageSelected(element) {
    selectedImage = element.options[element.selectedIndex].value;
    show_status("");
}

let selected_option = {
    id: "",
    value: ""
}
export function OptionSelected(element) {
    if (element.checked == true) {
        selected_option.id = element.id;
        selected_option.value = element.value;
    } else {
        selected_option.id = "";
        selected_option.value = "";
    }

    clear_options();
    // clear status
    show_status("");
};

export function ClearLog(el) {
    document.getElementById("action_log").textContent = "";
    system.Clear();
}

export function CloseWindow(el) {
    window.close();
}

export function RebootSystem(el) {
    document.getElementById("action_img").style.opacity = 0;
    if (selectedImage == "") {
        showActionPage('');
        updateActionMessage("Please select image");
        return;
    }
    let prompt = {
        title: "Reboot",
        message: "Continue with reboot?"
    };

    const img_info = JSON.parse(selectedImage);
    let disc_name = img_info.disc;
    let image_id = img_info.image;
    prompt.message = "Will reboot " + disc_name + " : image-" + image_id + ". Continue?"
    dialog.make(prompt).then(function(userOption) {
        if (userOption) {
            status = '<h2 style="color:#302e2e">Rebooting ' + disc_name + ' : ' + image_id + '</h2>'
            showActionPage(status);
            updateActionMessage("In 10 seconds");
            document.getElementById("action_log").textContent = "";
            system.Reboot(disc_name, image_id, 10);
        }
    });
}

export function TakeAction(element) {
    if (selectedImage.length == 0) {
        show_status(" Please select Image");
        return;
    }
    if (selected_option.id.length == 0) {
        show_status(" Please select any option");
        return;
    }
    let prompt = {
        title: "",
        message: "Action can't be undone. Continue?"
    };

    prompt.title = selected_option.value;
    dialog.make(prompt).then(function(userOption) {
        if (userOption) {
            let action = selected_option.value;
            const img_info = JSON.parse(selectedImage);
            let disc_name = img_info.disc;
            let image_id = img_info.image;
            status = '<h2>Running ' + action + ' on ' + disc_name + ' : ' + image_id + '</h2>'
            showActionPage(status);
            if (action == "Restore") {
                system.Restore(disc_name, image_id);
            } else if (action == "Factory-reset") {
                system.FactoryReset(disc_name, image_id);
            } else if (action == "Repair") {
                system.Repair(disc_name, image_id);
                sendMessage('resizeWindow');
            } else if (action == "Reboot") {
                system.Reboot(disc_name, image_id, 10);
                updateActionMessage("Rebooting in 10 sec");
            }
        }
    })
}

export function AbortAction(element) {
    let prompt = {
        title: "Cancel operation",
        message: "Are you sure ?"
    };

    dialog.make(prompt).then(function(userOption) {
        if (userOption) {
            hideOperationStatus();
        }
    });
}

let system = new System();
window.addEventListener('DOMContentLoaded', (event) => {
    updateScanStatus("Scan started");
    system.Scan();
});
