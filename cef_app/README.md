
## Build app
```
$ cd cef_app
$ mkdir build && cd build
$ cmake ../
$ make
```

### Run app
```
From build directory
$./Release/steamos-repair-app

App can be launched in 3 mode depending upon
passed command line args:
$./Release/steamos-repair-app  --fullscreen
      Launch in fullscreen mode, No Title bar

$./Release/steamos-repair-app  --maximized
      Launch in maximized state, Title bar present

$./Release/steamos-repair-app
      Launch with default size of (800*600)
```

